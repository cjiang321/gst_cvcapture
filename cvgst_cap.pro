TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    cvcapture_gstreamer.cpp

LIBS += \
    `pkg-config --libs opencv gstreamer-0.10               gstreamer-net-0.10\
                        gstreamer-app-0.10           gstreamer-netbuffer-0.10\
                        gstreamer-audio-0.10         gstreamer-pbutils-0.10\
                        gstreamer-base-0.10          gstreamer-plugins-base-0.10\
                        gstreamer-cdda-0.10          gstreamer-riff-0.10\
                        gstreamer-check-0.10         gstreamer-rtp-0.10\
                        gstreamer-controller-0.10    gstreamer-rtsp-0.10\
                        gstreamer-dataprotocol-0.10  gstreamer-sdp-0.10\
                        gstreamer-fft-0.10           gstreamer-tag-0.10\
                        gstreamer-floatcast-0.10     gstreamer-video-0.10\
                        gstreamer-interfaces-0.10    `

INCLUDEPATH += \
    /usr/include/gstreamer-0.10\
    /usr/include/glib-2.0\
    /usr/lib/i386-linux-gnu/glib-2.0/include\
    /usr/include/libxml2

HEADERS += \
    cvcapture_gstreamer.h
