#include <iostream>
#include <ctime>
#include <sstream>

#include <opencv2/opencv.hpp>

#include "cvcapture_gstreamer.h"

using namespace std;
using namespace cv;

int main(void)
{
    Matx33d K(5.8781933868326530e+02, 0, 3.3127983417951896e+02,
              0, 5.8910513442917556e+02, 2.3486662037824394e+02,
              0, 0, 1);

    float a[] = {1.1132964927611745e-02, 2.4454804016328133e-01,
                -6.1141627364070666e-03, 8.7006706565327641e-04,
                -1.3041149315466041e+00};
    Mat distortion = Mat(5, 1, CV_32F, a);


    CvCapture_GStreamer gst_cap;

    int type = CV_CAP_GSTREAMER_IMXV4L;
    //int type = CV_CAP_GSTREAMER_V4L2;

    if (!gst_cap.open(type, NULL))
    {
        cout << "fail open device with gstreamer" << endl;
        return -1;
    }
    
    //if (!gst_cap.setProperty(CV_CAP_PROP_FPS, 60))
    //    cout << "failed setting fps" << endl;

    for (int i = 0; i < 20; i++){
        gst_cap.grabFrame();
        waitKey(100);
        gst_cap.retrieveFrame(0);
    }

    IplImage *iplim;
    Mat im;
    //Mat imd;
    int i = 0;
    int j = 0;

    double state;
    state = gst_cap.getProperty(CV_CAP_PROP_FRAME_HEIGHT);
    cout << "frame height is: " << state << endl;
    state = gst_cap.getProperty(CV_CAP_PROP_FRAME_WIDTH);
    cout << "frame width is:  " << state << endl;
    state = gst_cap.getProperty(CV_CAP_PROP_FPS);
    cout << "current fps is:  " << state << endl;
    
    time_t startt, endt;
    time(&startt);
    char c = 1;

    while ((c != 'e') && (i > -1))
    {
        gst_cap.grabFrame();

        iplim = gst_cap.retrieveFrame(1);

        im = Mat(iplim);

        imshow("gst", im);

        //undistort(im, imd, Mat(K), distortion);

        //imshow("undistorted", imd);

        i++;

        c = waitKey(10);

        if (c == 'g')
        {
            j++;
            ostringstream convert;
            convert << j;
            string filename = "cap" + convert.str() + ".bmp";
            imwrite(filename, im);
        }

    }

    time(&endt);
    double secs = difftime(endt, startt);

    cout << "Average frame rate is: " << i / secs << endl;

    gst_cap.close();

    destroyAllWindows();

    return 0;
}
